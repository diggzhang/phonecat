Git global setup

	git config --global user.name "diggzhang"
	git config --global user.email "diggzhang@gmail.com"

Create a new repository

	mkdir phonecat
	cd phonecat
	git init
	touch README.md
	git add README.md
	git commit -m "first commit"
	git remote add origin git@gitlab.com:diggzhang/phonecat.git
	git push -u origin master

Push an existing Git repository

	cd existing_git_repo
	git remote add origin git@gitlab.com:diggzhang/phonecat.git
	git push -u origin master


